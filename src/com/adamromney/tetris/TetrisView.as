package com.adamromney.tetris
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	import spark.components.Button;
	import mx.collections.ArrayList;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	
	/**
	 * Used for visualizing the gameplay space, i.e. grid.  A bitmap containing
	 * the individual blocks used to compose a tetrimino (Tetris game piece) is
	 * copied to a background in various positions to animate a tetrimino falling
	 * into place.  The technique of blitting and backbuffering is used to render
	 * the blocks quickly and with few CPU resources, though at greater cost of
	 * memory.
	 * 
	 * @property _background	bitmap representing background of gameplay space
	 * @property _blocks		bitmap containing images of blocks use to make up
	 * 								tetrimino shapes
	 * @property _blockWidth	height of block
	 * @property _blockHeight	width of block
	 * @property _model			game model to control updates and game logic
	 * @property _backgroundRect	rectangle representing background
	 * @property _created		flag to notify when CREATION_COMPLETE event has been
	 * 								detected, used in scaling background and images
	 * 
	 * @author Adam Romney
	 * 
	 * Sources:
	 * http://www.adobe.com/devnet/flex/articles/actionscript_blitting.html
	 * http://englishblog.flepstudio.org/tutorials/scaling-an-image-resizing-a-bitmap/
	 */
	public class TetrisView extends UIComponent
	{
		private var _background:Bitmap = new Bitmap(); //All shapes will be drawn onto this
		private var _blocks:Bitmap = new Bitmap();
		private var _blockWidth:Number;
		private var _blockHeight:Number;
		private var _model:TetrisModel;
		private var _backgroundRect:Rectangle;
		private var _created:Boolean;						
				
		/**
		 * Constructor
		 * Instantiates model, loads images and sets height.
		 */
		public function TetrisView()
		{
			super();
			this.percentHeight = 100;
			this.percentWidth = 100;
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE, viewCreationComplete);
			this.addEventListener(FlexEvent.UPDATE_COMPLETE, viewInitialize);
			this.addEventListener(FlexEvent.BUTTON_DOWN, handleButtonPress);
			
			//Load tetrimino block images
			var imageLoader:Loader = new Loader();
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, imageLoadComplete);
			imageLoader.load(new URLRequest("assets/allBlocks.png"));			
			
			this._background.bitmapData = new BitmapData(400, 800, false, 0x000000);
			this.addChild(_background);						
			
			
			//Create model
			_model = new TetrisModel();
		}
		
		/**
		 * Ensure that view has been created before attempting to size display 
		 * objects.
		 * @param event CREATION_COMPLETE event for this UIComponent
		 */
		private function viewCreationComplete(event:Event):void{
			
			_created = true;			
		}
		
		/**
		 * Probably a hack.  Used to tag along the updateComplete event after parent, 
		 * and thus, this instance of the TetrisView object has received it's true size
		 * @param event  
		 */
		private function viewInitialize(event:Event):void{
			if(_created && this.height != 0){
				ensureAspectRatio();
				this.addEventListener(Event.ENTER_FRAME, onFrameEnter);				
				//Remove this listener as it is no longer needed
				this.removeEventListener(FlexEvent.UPDATE_COMPLETE, viewInitialize);
			}
		}
		
		/**
		 * Responsible for loading the template image for block
		 * for tetrimino blocks.
		 * @param event dispatched imageLoadComplete event 
		 */
		private function imageLoadComplete(event:Event):void {
			
			var tempBitmapData:BitmapData = event.target.content.bitmapData;
			
			_blocks.bitmapData = new BitmapData(tempBitmapData.width, tempBitmapData.height);
			_blocks.bitmapData.copyPixels(tempBitmapData, new Rectangle(0,0, _blocks.width, _blocks.height), new Point(0,0));
			
		}//END imageLoadComplete(Event)
		
		/**
		 * Determines a width and height that ensures a 1:2 aspect ratio for
		 * the grid.  The scale factor derived here is also used to scale the
		 * blocks used to visualize tetriminoes.
		 */
		private function ensureAspectRatio():void{
			
			var scaleFactor:Number;
			var scaleMatrix:Matrix = new Matrix();
			var scaledBitmapData:BitmapData;
			var bgWidth:Number;
			var bgHeight:Number;
			
			//Use multiples of 10 for cleaner scaling
			var i:Number = this.height - (this.height % 10);
			for(i; i > 0; i-= 10){
				if(i / 2 <= this.width){
					
					bgWidth = i / 2;
					bgHeight = i;
					break;
				}
			}
			
			//Scale the background
			scaledBitmapData = new BitmapData(bgWidth, bgHeight, false, 0x000000);
			scaleFactor = bgWidth / _background.width;
			scaleMatrix.scale(scaleFactor, scaleFactor);
			scaledBitmapData.draw(_background.bitmapData, scaleMatrix);
			_background.x = 0;//(this.width - bgWidth) / 2;
			_background.y = (this.height - bgHeight) / 2;
			_background.bitmapData = scaledBitmapData;
			_backgroundRect = new Rectangle(0, 0, bgWidth, bgHeight);
			
			//Scale the blocks
			scaledBitmapData = new BitmapData(_blocks.width * scaleFactor, _blocks.height * scaleFactor, false, 0x000000);
			scaledBitmapData.draw(_blocks.bitmapData, scaleMatrix);
			_blocks.bitmapData = scaledBitmapData;
			this._blockHeight = _blocks.height / 3;
			this._blockWidth = _blocks.width / 3;			
		}
		
		/**
		 * The main game loop.  Polls for user input and changes in game state.
		 * @param evt
		 */
		private function onFrameEnter(evt:Event):void{
					
			draw();		
			_model.update();	
		}
		
		/**
		 * Responsible for drawing tetriminoes on background and providing
		 * visualization for the user.
		 */
		public function draw():void{
			
			_background.bitmapData.lock();
			renderGrid();
			_background.bitmapData.unlock();
		}		
		
		/**
		 * Renders the state of the grid, i.e. placement of all 
		 * tetrimino blocks.
		 */
		private function renderGrid():void{
			var coordsCollection:Object = _model.filledCoordinates;
						
			_background.bitmapData.fillRect(_backgroundRect, 0x000000);
			
			for(var row:* in coordsCollection){
				for(var col:* in coordsCollection[row]){
					//Choose the color of the block
					switch(coordsCollection[row][col]){
						
						case "I":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(0,_blockHeight * 2, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);							
							break;
						case "J":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(0,0, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);
							break;
						case "L":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(_blockWidth, _blockHeight, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);
							break;
						case "O":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(_blockWidth,0, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);
							break;
						case "S":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(0,_blockHeight, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);
							break;
						case "T":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(_blockWidth * 2, _blockHeight, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);
							break;
						case "Z":
							_background.bitmapData.copyPixels(
								_blocks.bitmapData, 
								new Rectangle(_blockWidth * 2,0, _blockWidth, _blockHeight),
								new Point(col * _blockWidth, row * _blockHeight),
								null,null, false);
							break;
					}
				}
			}		
		}
		
		/**
		 * Handles user input during gameplay session.
		 */
		public function handleButtonPress(event:Event):void{
			
			var parent:* = event.target.parent;
			var childIndex:Number = event.target.parent.getElementIndex(event.target);
			var child:*;
			
			/*
			//Disable all buttons but the origin of the button presse event
			for(var i:Number = 0; i < parent.numElements; i++){
				
				if(childIndex == i){
					continue;	
				}
				
				if(parent.getElementAt(i).hasOwnProperty("id")){
					
					child = parent.getElementAt(i);
					
					if(child.id == "left" || 
						child.id == "rotate" || 
						child.id == "right"){
						
						child.enabled = false;
					}					
				}				
			}
			*/
			switch(event.target.id){
				case "left":
					_model.sign = -1;
					break;
				case "right":
					_model.sign = 1;
					break;				
				case "rotate":
					_model.rotate = true;
					break;
			}			
		}
		
		/**
		 * Renables all buttons.  Resets frame counter for model.
		 */
		public function handleButtonRelease(event:Event):void{
			
			var parent:* = event.target.parent;
			var child:*;
			
			//Enable all buttons
			for(var i:Number = 0; i < parent.numElements; i++){
				
				if(parent.getElementAt(i).hasOwnProperty("id")){
					
					child = parent.getElementAt(i);
					
					if(child.id == "left" || 
						child.id == "rotate" || 
						child.id == "right"){
						
						child.enabled = true;
					}					
				}				
			}
				
			//Set transformer variables to neutral
			_model.rotate = false;
			_model.sign = 0;
			_model.resetInputFrameCounter();
			
		}		
	}//END class TetrisView
}//END package
