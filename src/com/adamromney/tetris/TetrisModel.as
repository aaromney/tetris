// ActionScript file
package com.adamromney.tetris{	
		

	import mx.collections.ArrayList;
	import flash.desktop.NativeApplication;
		
	/**
	 * Controls game logic and tetrimino positions.  A tetrimino is a game piece
	 * used in Tetris that is composed of 4 blocks.  There are 7 tetrimonoes in
	 * total designated by the characters that they resemble, e.g. characters of
	 * the string "IJLOSTZ".  One tetrimino 'falls' through the game space, i.e.
	 * the grid, at a time.  The player may only move the tetrimino left or right,
	 * or rotate it while it falls until coming to contact with some surface from
	 * below.
	 *   
	 * Uses rules as described by sources:
	 * http://tetris.wikia.com/wiki/Line_clear
	 * http://en.wikipedia.org/wiki/Tetris
	 * 
	 * @property _tetriminoes	String containing characters associated with
	 * 								tetrimino shapes
	 * @property _currentShape	The shape that is current falling
	 * @property _nextShape		The shape that will be loaded next
	 * @property _tetriminoCoordinates	The 4 coordinates that designate the
	 * 									location of the upper left corner of
	 * 							   		each block that makes up a tetrimino
	 * @property _grid	The grid that represents the playing space; planned to 
	 * 					be replaced eventually with Object instance _filledCoordinates
	 * @property _gridRowValues	The values corresponding to the number of blocks that
	 * 								are currently residing in a row
	 * @property _filledCoordinates	Object that is passed to the view containing all data
	 * 								 on the _grid; can most likely be used to replace grid
	 * 								but will passing this hurt encapsulation?
	 * @property _numberFramesPerStep	Number of frames per drop of a shape.  In release mode,
	 * 									this would increase as the player scored more points
	 * @property _frameCounter	Counter for keeping track of current frame
	 * @property _inputFrameCounter		Tracker used to limit repeat called to rotate or shift
	 * @property _numberFramesPerInput	Interval used to limit repeat called to rotate or shift;
	 * 									modify this to allow player to shift or rotate more frequently
	 * 									while holding down a button
	 * @property _dropRowFrameOffset	Interval during which a row or rows are cleared;
	 * 									allows player to observe grid update
	 * @property _rowsToClear	Collection of row numbers corresponding to those that
	 * 							need to be cleared.  In Tetris, this is never more than
	 * 							4 rows at a time.
	 * @property _score			Not used in this implementation.
	 * @property _signShift		1 for right, -1 for left
	 * @property _rotate		Whether the player wants to rotate
	 * @property _rotationHelper	Class to handle non-elegant rotation code
	 * @property _INIT_CONFIG_I		Initial configuration for "I"
	 * @property _INIT_CONFIG_J		Initial configuration for "J"
	 * @property _INIT_CONFIG_L		Initial configuration for "L"
	 * @property _INIT_CONFIG_O		Initial configuration for "O"
	 * @property _INIT_CONFIG_Z		Initial configuration for "Z"
	 * @property _INIT_CONFIG_S		Initial configuration for "S"
	 * @property _INIT_CONFIG_T		Initial configuration for "T"
	 * 
	 * @author Adam Romney
	 * 
	 * Sources:  
	 * 
	 * Please see www.diigo.com/aaromney for various sources, otherwise...
	 * 
	 * Real World Flash Game Development, 2nd ed. Christopher Griffith
	 * Essential Actionscript 3.0, 3rd ed. Colin Moock 
	 */
	public class TetrisModel {
		
		private var _tetriminoes:String = "IJLOSTZ";
		private var _currentShape:String;
		private var _nextShape:String;
		private var _tetriminoCoordinates:Array = new Array(4);
		private var _grid:Array = new Array(20);
		private var _gridRowValues:Array = new Array(20);
		private var _filledCoordinates:Object = new Object();
		private var _numberFramesPerStep:Number = 6;
		private var _frameCounter:Number = 0;
		private var _inputFrameCounter:Number = 0;
		private var _numberFramesPerInput:Number = 24;
		private var _dropRowFrameOffset:Number = -6;
		private var _rowsToClear:Array = new Array();
		private var _score:Number = 0;
		private var _signShift:Number = 0;
		private var _rotate:Boolean = false;
		private var _rotationHelper:RotationHelper = new RotationHelper(_grid, 
						_filledCoordinates);
		private const _INIT_CONFIG_I:Array = [[0,5],
								      [0,4],
									  [0,6],
									  [0,7]];
		private const _INIT_CONFIG_J:Array = [[0,5],
									 [0,4],
									 [0,6],
									 [1,6]];
		private const _INIT_CONFIG_L:Array = [[0,5],
									 [0,4],
									 [1,4],
									 [0,6]];
		private const _INIT_CONFIG_O:Array = [[0,4],
									 [0,5],
									 [1,4],
									 [1,5]];;
		private const _INIT_CONFIG_Z:Array = [[0,5],
									 [0,4],
									 [1,5],
									 [1,6]];
		private const _INIT_CONFIG_S:Array = [[0,5],
									 [1,5],
									 [1,4],
									 [0,6]];
		private const _INIT_CONFIG_T:Array = [[0,5],
								     [0,4],
								     [0,6],
								     [1,5]];
		
		/**
		 * Constructor
		 */
		public function TetrisModel(){
			
				init();			
			
		}//END TetrisModel()
		
		/**
		 * Initializes various properties.  Eventually used for restarting.
		 */
		private function init():void{
			
			//Initialize grid for 20 rows, 10 columns
			for(var i:Number = 0; i < 20; i++){
				
				_grid[i] = new Array(10);
				_filledCoordinates[i] = new Object();
			}
			
			//Initialize row values
			for(var i:Number = 0; i < _gridRowValues.length; i++){
				_gridRowValues[i] = 0;
			}
			
			//Initialize tetriminoCoordinates
			for(i = 0; i < 4; i++){
				_tetriminoCoordinates[i] = new Array(2);
			}
			//Initialize first tetrimino to be loaded
			_nextShape = _tetriminoes.charAt(Math.floor(Math.random() * 7));			
			
			
			
			loadTetrimino();
		}
		
		/**
		 * Updates grid space and checks game logic.
		 */
		public function update():void{
			
			++this._frameCounter;
			
			//Disable user input while showing cleared rows
			if(this._frameCounter > 0){
				
				//Check for user input to shift left or right			
				if(_signShift != 0){
					if(_inputFrameCounter % _numberFramesPerInput == 0){
						if(shiftAllowed()){
							translateTetrimino(0, _signShift);
						}
					}
					_inputFrameCounter++;
				}			
				
				//Check for use input to rotate
				if(_rotate){
					if(_inputFrameCounter % _numberFramesPerInput == 0){
						_rotationHelper.rotate(_currentShape, _tetriminoCoordinates);
					}
					_inputFrameCounter++;
				}			
			}
			
			//Shift tetrimino down one step
			if(this._frameCounter >= this._numberFramesPerStep){
				
				this._frameCounter = 0;
				
				//If there is room to drop the tetrimino
				if(dropAllowed()){
				
					translateTetrimino(1,0);					
				}
				//End game if there is no room and tetrimino was just loaded
				else if(_tetriminoCoordinates != null && 
							_tetriminoCoordinates[0][0] == 0){
					//End game
					endGame();
				}
				//Clear rows as necessary
				else if(clearedRows(_tetriminoCoordinates)){
					_frameCounter += _dropRowFrameOffset;
				}
				//Check row values and load a new tetrimino
				else{
					
					loadTetrimino();
				}								
			}		
			
			//Adjust grid for missing rows
			if(_frameCounter == Math.floor(_dropRowFrameOffset / 2)){
				dropRows();
			}
			
		}//END update()
		
		/*****************************************
		 * Deal with updates imposed by user input
		 ****************************************/
		
		/**
		 * Check whether tetrimino can drop a row
		 * @return true if space is clear to drop
		 */
		private function dropAllowed():Boolean{
			
			var row:Number;
			var col:Number;
			
			//There is no tetrimino currently loaded
			if(_tetriminoCoordinates == null){
				return false;
			}
			
			for(var i:Number = 0; i < _tetriminoCoordinates.length; i++){
				row = _tetriminoCoordinates[i][0];
				col = _tetriminoCoordinates[i][1];
				
				//If reached the bottom
				if(row + 1 >= 20){
					
					countRowValues();
					return false;
				}
				//If the space below this block is occupied AND
				else if(_grid[row + 1][col] != undefined &&
					//The space below this block is not in the same tetrimino
					(function foundUnrelatedBlock():Boolean{
						for each(var j:* in _tetriminoCoordinates){
							if(j[0] == row + 1 && j[1] == col){
								
								return false;
							}
						}
						
						return true;})()){
					
					//No space available
					countRowValues();
					return false;
				}				 
			}
			
			//Space is clear for tetrimino to drop
			return true;
		}		
		
		/**
		 * Check whether tetrimino can shift a col
		 * @return true if space is clear to shift
		 */
		private function shiftAllowed():Boolean{
			
			var row:Number;
			var col:Number;
			
			if(_tetriminoCoordinates == null){
				return false;
			}
			
			for(var i:Number = 0; i < _tetriminoCoordinates.length; i++){
				row = _tetriminoCoordinates[i][0];
				col = _tetriminoCoordinates[i][1];
				
				//If reached left or right bounds
				if(col + _signShift < 0 || col + _signShift > 9){
					return false;
				}
					//If the space next to this block is occupied AND
				else if(_grid[row][col + _signShift] != undefined &&
					//The space next to this block is not in the same tetrimino
					(function foundUnrelatedBlock():Boolean{
						for each(var j:* in _tetriminoCoordinates){
							if(j[0] == row  && j[1] == col + _signShift){
								
								return false;
							}
						}
						
						return true;})()){
					
					//No space available
					
					return false;
				}				 
			}
			
			//Space is clear for tetrimino to drop
			return true;
		}
		
		/**
		 * Translates the blocks in a tetrimino down by 1
		 */
		private function translateTetrimino(deltaRow:Number, deltaCol:Number):void {
			
			//Erase old position
			for(var i:Number = 0; i < 4; i++){
				
				//Delete previous position from grid
				delete _grid[_tetriminoCoordinates[i][0]][_tetriminoCoordinates[i][1]];
								
				//Delete position from filledCoordinates 
				delete _filledCoordinates[_tetriminoCoordinates[i][0]][_tetriminoCoordinates[i][1]];								
			}
			
			//Update new coordinates
			for(i = 0; i < 4; i++){
				
				//Update grid
				_grid[_tetriminoCoordinates[i][0]+= deltaRow][_tetriminoCoordinates[i][1] += deltaCol] = this._currentShape;
				
				//Add a reference for the renderer
				if(_filledCoordinates[_tetriminoCoordinates[i][0]] == undefined){
					_filledCoordinates[_tetriminoCoordinates[i][0]] = new Object();
				}
				_filledCoordinates[_tetriminoCoordinates[i][0]][_tetriminoCoordinates[i][1]] = this._currentShape;
			}								
		}		
		
		/**
		 * Randomly selects a tetrimino shape
		 */
		private function chooseTetrimino():void{
		
			this._currentShape = this._nextShape;
			this._nextShape = this._tetriminoes.charAt(Math.floor(Math.random() * 7));
						
		}
		
		/**
		 * Sets a tetrimino starting coordinates on grid
		 */
		private function loadTetrimino():void{
			//Choose a random character, i.e. tetrimino shape
			switch(this._nextShape){
				case "I":
					_tetriminoCoordinates = Clone.clone(_INIT_CONFIG_I);
					break;
				case "J":
					_tetriminoCoordinates = Clone.clone( _INIT_CONFIG_J);
					break;
				case "L":
					_tetriminoCoordinates = Clone.clone(_INIT_CONFIG_L);
					break;
				case "O":
					_tetriminoCoordinates = Clone.clone(_INIT_CONFIG_O);
					break;
				case "S":
					_tetriminoCoordinates = Clone.clone(_INIT_CONFIG_S);
					break;
				case "T":
					_tetriminoCoordinates = Clone.clone(_INIT_CONFIG_T);
					break;
				case "Z":
					_tetriminoCoordinates = Clone.clone(_INIT_CONFIG_Z);
					break;
			}
			
			//Load the tetrimino in it's starting place on the grid
			for(var i:Number=0 ; i < 4; i++){
				_grid[_tetriminoCoordinates[i][0]][_tetriminoCoordinates[i][1]];
				if(_filledCoordinates[_tetriminoCoordinates[i][0]] == undefined){
					_filledCoordinates[_tetriminoCoordinates[i][0]] = new Object();
				}
				_filledCoordinates[_tetriminoCoordinates[i][0]][_tetriminoCoordinates[i][1]] = _nextShape;
			}
			
			//Reset rotation config to default
			_rotationHelper.reset();
			chooseTetrimino();
		}
		
		/**
		 * Counts number of blocks in a row
		 */
		private function countRowValues():void{
			var row:Number;
			
			for(var i:Number = 0; i < _tetriminoCoordinates.length; i++){
				row = _tetriminoCoordinates[i][0];
				++_gridRowValues[row];
				if(_gridRowValues[row] >= 10){
					//Collect number of rows to clear
					_rowsToClear.push(row);
				}
			}
			
		}
		
		/**
		 * Clears any rows that are eligible
		 * @return true if rows were cleared
		 */
		private function clearedRows(coordinates:Array):Boolean{
			
			var cleared:Boolean = false;
			
			//If there are any rows to clear
			if(_rowsToClear.length > 0){
				
				//Set flag
				cleared = true;
				
				for each( var i:* in _rowsToClear){
					
					//Reset the row value
					_gridRowValues[i] = 0;
					
					//Clear the row
					for(var j:Number = 0; j < _grid[i].length; j++){
						delete _grid[i][j];
						delete _filledCoordinates[i][j];
					}
				}
				
				_tetriminoCoordinates = null;
			}			
			
			return cleared;
		}
		
		/**
		 * Translates the entire grid down by the number of rows cleared.
		 * Provides the 'gravity' for the grid when rows have cleared.
		 * Rows cleared may not always be adjacent.
		 */
		private function dropRows():void{
			
			var targetRow:Number = 0; //Row to be copied
			var destinationRow:Number; //Row that will recieve copy
			var deltaRow:Number; //Number of rows to drop in a given step
			var index:Number = 0; //Index in rows to clear
			
			//Sort array
			_rowsToClear.sort(Array.DESCENDING);
			
			
			while(_rowsToClear.length > 0){
				
				destinationRow = _rowsToClear[index];
				deltaRow = deltaDrop(index);
				
				if(deltaRow > 1){
					_rowsToClear.splice(index + 1, deltaRow - 1);	
				}
				
				while(destinationRow >= deltaRow){
					targetRow = destinationRow - deltaRow;
					
					_grid[destinationRow] = Clone.clone(_grid[targetRow]);
					_filledCoordinates[destinationRow] = Clone.clone(_filledCoordinates[targetRow]);
					_gridRowValues[destinationRow] = _gridRowValues[targetRow];
					
					destinationRow--;
				}
				
				_rowsToClear.splice(index, 1);
			}
			
			_rowsToClear = [];
		}
		
		/**
		 * Checks if cleared rows are adjacent, thus determining the number
		 * of rows to be dropped in the grid.
		 * @param index	The lowest point in the grid that the contents above it
		 * 						will drop to
		 * @return delta	The number of rows dropped
		 */
		private function deltaDrop(index:Number):Number{
			var currentRow:Number = index;
			var nextRow:Number = index + 1;
			var delta:Number = 1;
			
			while(nextRow < _rowsToClear.length && 
				_rowsToClear[currentRow] - _rowsToClear[nextRow] == 1){
				
				delta++;
				currentRow = nextRow; 
				nextRow++;
			}
			
			return delta;
			
		}
		
		/**
		 * Eventually should tell ViewNavigator to push the 'menu' view
		 * that will allow the player to retry or quit the game.  Currently
		 * just exits the application.
		 */
		private function endGame():void{
			trace("End game!");
			NativeApplication.nativeApplication.exit();
			
			
		}		
		
		/**
		 * Accessor method for collection of coordinates to be used by Tetris renderer
		 */
		public function get filledCoordinates():Object{return _filledCoordinates;}
		
		/**
		 * Setter methods
		 */
		public function set sign(value:Number):void {_signShift = value;};
		public function set rotate(value:Boolean):void {_rotate = value;};
		public function resetInputFrameCounter():void{_inputFrameCounter = 0;};
		
		/**
		 * Returns the contents of the grid at coordinates x and y
		 * @param	coords	Array of length 2 containing (x,y)
		 * @return 	contents of grid at (x,y)
		 */
		public function getBlock(coords:Array):String{return _grid[coords[0]][coords[1]];}
	}//END class TetrisModel
}