package com.adamromney.tetris
{
	import flash.utils.ByteArray;
	
	public class Clone
	{
		
		/**
		 * Copied and pasted directly from Adobe. 
		 * For use with copying contents of multidimensional array.    
		 */
		public static function clone(source:Object):* 
		{ 
			var myBA:ByteArray = new ByteArray(); 
			myBA.writeObject(source); 
			myBA.position = 0; 
			return(myBA.readObject()); 
		}
	}
}